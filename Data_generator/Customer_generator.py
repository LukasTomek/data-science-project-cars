import numpy as np
import pandas as pd
import random
import seaborn as sns
import string

def generate_customer_id(no_customers, len_id=9, seed=42):
    '''
    Generates unique IDs for each client
    '''
    # Global rand function to generate random samples based on given distribution
    global rand
    rand = np.random.RandomState(seed)
    
    # Generate unique IDs
    ids = []
    while len(ids) < no_customers:
        id = ''.join(random.choice(string.digits) for pos in range(len_id))
        if id not in ids:
            ids.append(''.join(random.choice(string.digits) for pos in range(len_id)))
            
    # Return list of unique IDs of each customer
    return ids

def generate_customer_age(no_customers):
    '''
    Generates age for each customer based on randomized beta distribution
    '''
    # Distribution parameters
    a = rand.uniform(1.5,3,1)
    b = rand.uniform(1.5,3,1)
    
    # Generate ages based on distribution and function transformation
    ages_dist = rand.beta(a, b, no_customers)
    ages_list = (ages_dist**1/2.5)*150+18
    
    # Return list of integers
    return ages_list.astype(int)

def generate_education(no_customers,
    education_list = ['Elementary School', 'High School', 'College Degree', 'Doctorate Degree'], 
    education_probs = [0.1, 0.45, 0.4, 0.05]):
    '''
    Generates list of education based on list of education levels and their respective probabilities
    '''
    edu_list = []
    for row in range(no_customers):
        edu_list.append(random.choices(education_list, education_probs))
    return edu_list

def generate_income(no_customers, customer_dataset, income_basis=[15_000, 40_000, 70_000, 95_000], 
    education_list = ['Elementary School', 'High School', 'College Degree', 'Doctorate Degree']):
    '''
    Generates random in comes based on list of income levels corresponding to list of education levels, 
    randomized by a coefficient generated from gamma distribution.
    '''
    # Table of income levels
    income_levels = pd.concat([pd.Series(income_basis), pd.Series(education_list)], axis=1)
    income_levels = pd.DataFrame(income_levels)
    income_levels.columns = ['income_basis','education_list']

    # Join income levels to customer data
    customer_dataset = customer_dataset.merge(income_levels, how='left', on='education_list')

    # Randomize income based on gamma distribution
    random_list = rand.gamma(5, 3, no_customers)/10
    customer_dataset['random_income'] = pd.Series(random_list)
    customer_dataset['annual_income'] = customer_dataset['random_income'] * customer_dataset['income_basis']
    customer_dataset['annual_income'] = customer_dataset['annual_income'].astype(int)

    # Return dataset with annual income of each customer
    return customer_dataset
